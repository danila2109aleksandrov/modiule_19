﻿#include <iostream>
using namespace std;

class Animal
{
public:
    virtual void Voice()
    {
        cout << "Voices" << endl;
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        cout << "Dog : Woof" << endl;
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        cout << "Cat :Meow" << endl;
    }
};

class BabyShark : public Animal
{
public:
    void Voice() override
    {
        cout << "BabyShark :Tutururu" << endl;
    }
};

int main()
{
    int n = 5;
    Animal** mas = new Animal*[n];
    for (int i = 0; i < 1; i++)
    {
        mas[i] = new Animal();
        mas[i + 1] = new Dog();
        mas[i + 2] = new Cat();
        mas[i + 3] = new BabyShark();
    }
    for (int i = 0; i < n-1; i++)
    {
        mas[i]->Voice();
    }
    return 0;
}

